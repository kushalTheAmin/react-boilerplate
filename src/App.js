import React from 'react'
import { hot } from 'react-hot-loader'

class App extends React.Component {
  render() {
    return (
      <div>
        <h1>High Performance Web Development!</h1>
        <p> Part of learning path...</p>
      </div>
    )
  }
}

export default hot(module)(App)
